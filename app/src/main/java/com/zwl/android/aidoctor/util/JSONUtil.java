package com.zwl.android.aidoctor.util;

import java.util.List;
import java.util.Map;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSON;

/**
 * @author tcmaiok.org
 */
public class JSONUtil {

    /**
     * 将List转换为JSON格式字符。
     *
     * @param ls
     * @return
     */
    public static String list2JSONString(List ls) {
        String jsonString = "";

        jsonString = JSON.toJSONString(ls, false); // 是否格式化

        return jsonString;
    }

    /**
     * 将JSON格式字符转换为List。
     *
     * @param jsonStr
     * @return
     */
    public static List jsonString2List(String jsonStr) {
        //jsonStr = jsonStr.replaceAll("\\\\", "");
        List ls = null;
        try {
            ls = JSON.parseArray(jsonStr, Object.class);
        } catch (Exception e) {
            System.err.println("jsonString2List 错误：" + e.getMessage());
        }

        return ls;
    }

    /**
     * 将JSON格式字符串反序列化为JSONArray。
     *
     * @param jsonStr
     * @return
     */
    public static JSONArray jsonStr2JSONArray(String jsonStr) {
        JSONArray jsa = null;
        try {
            jsa = JSON.parseArray(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsa;
    }

    /**
     * @param jsonStr
     * @param colName
     * @return
     */
    public static String getJSONArrayObj0ColValue(String jsonStr, String colName) {
        JSONArray jsa = jsonStr2JSONArray(jsonStr);
        JSONObject jso = (JSONObject) jsa.get(0);
        String retStr = "";
        try {
            retStr = jso.get(colName).toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "json Formate Error!";
        } finally {
            jsa = null;
            jso = null;
        }
        return retStr;
    }

    /**
     * map转json格式字符串
     *
     * @param map
     * @return
     */
    public static String map2Json(Map map) {
        String jsonText = JSON.toJSONString(map, false);
        return jsonText;
    }

    /**
     * json格式字符串 转map
     *
     * @param json
     * @return
     */
    public static Map json2Map(String json) {
        Map<String, String> map = (Map<String, String>) JSON.parse(json);
        return map;
    }

    /**
     * 字符数组转json格式字符串
     *
     * @param arr
     * @return
     */
    public static String array2JsonStr(String[] arr) {
        String jsonText = JSON.toJSONString(arr, true);
        return jsonText;
    }

    /**
     * json格式字符串转数组
     *
     * @param jsonText
     * @return
     */
    public static JSONArray json2Array(String jsonText) {
        JSONArray jsonArr = JSON.parseArray(jsonText);
        return jsonArr;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        String jsonStr = "[{\"maxCallDateNum\" : \"1297316173718\"}]";
        String jsonStrMap = "{\"maxCallDateNum\" : \"1297316173718\",\"phoneNum\" : \"189\"}";
        JSONArray jsa = jsonStr2JSONArray(jsonStr);
        JSONObject jso = (JSONObject) jsa.get(0);

        try {
            System.out.println(jso.get("maxCallDateNum").toString() + "\n");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Map map = json2Map(jsonStrMap);
        System.out.println(map.size());
        System.out.println(map2Json(map));
    }
}
