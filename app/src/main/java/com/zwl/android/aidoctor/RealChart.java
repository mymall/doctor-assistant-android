package com.zwl.android.aidoctor;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tcmaiok.org
 * 脉象实时图像
 */
public class RealChart extends View {
    private List mPointList = new ArrayList();
    private int mPointY = 0;
    private Paint mPointPaint = new Paint();   //画笔

    public RealChart(Context context, AttributeSet attrs) {
        // TODO Auto-generated constructor stub
        this(context, attrs, 0);
    }

    public RealChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //初始化画笔
        mPointPaint.setColor(getContext().getResources().getColor(android.R.color.holo_red_dark));
        mPointPaint.setStrokeWidth(2.0f);
        mPointPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas paramCanvas) {
        super.onDraw(paramCanvas);
        //mPointY = (int) (Math.random() * 100);
        if (mPointList.size() >= 2) {
            for (int k = 0; k < -1 + mPointList.size(); k++) {
                paramCanvas.drawLine(((Point) mPointList.get(k)).x,
                        ((Point) mPointList.get(k)).y,
                        ((Point) mPointList.get(k + 1)).x,
                        ((Point) mPointList.get(k + 1)).y, mPointPaint);
            }
        }
        Point localPoint1 = new Point(getWidth(), 0xff * 3 - mPointY * 3);//最右边的新点
        int i = mPointList.size();
        int j = 0;
        if (i > 400) {     //最多绘制400个点，多余的点移除队列
            mPointList.remove(0);//移除最左边的点
            while (j < 400) {
                Point localPoint3 = (Point) mPointList.get(j);
                localPoint3.x = (-3 + localPoint3.x);
                j++;
            }
            mPointList.add(localPoint1);
            return;
        }

        while (j < mPointList.size()) {    //每新产生使前面的每一个点左移3像素
            Point localPoint2 = (Point) mPointList.get(j);
            localPoint2.x = (-3 + localPoint2.x);
            j++;
        }
        mPointList.add(localPoint1);
    }

    public final void ClearList() {
        mPointList.clear();
    }

    public final void addPointToList(int paramInt) {
        mPointY = paramInt;
        invalidate();//重绘
    }

    public void stop() {
        mPointList.clear();
        invalidate();
    }
}
