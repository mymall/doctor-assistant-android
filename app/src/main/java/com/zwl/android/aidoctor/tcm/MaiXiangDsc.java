package com.zwl.android.aidoctor.tcm;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @author tcmaiok.org
 */
public class MaiXiangDsc {
    public static LinkedHashMap<String,String> mxMap = new LinkedHashMap<String,String>();
    public static String[] mxArr = new String[28];
    static {
        mxMap.put("浮","轻取即得，重按稍减而不空。表证，亦主虚证。");
        mxMap.put("洪","指下宽大如波涛汹涌，来盛去衰。热邪亢盛。");
        mxMap.put("濡","浮而细软。主虚，又主湿。");
        mxMap.put("散","浮散无根，稍按则无，至数不齐。元气离散，脏腑之气将绝。");
        mxMap.put("芤","浮大中空，如按葱管。失血伤阴。");
        mxMap.put("革","弦急中空，如按鼓皮。精血虚寒。");
        mxMap.put("沉","轻取不应，重按始得。里证。");
        mxMap.put("伏","重手推筋按骨始得，甚则伏而不见。邪闭，厥证，痛极。");
        mxMap.put("牢","沉按实大弦长。阴寒内实，疝气，癥瘕。");
        mxMap.put("迟","脉来迟慢，一息不足四至。寒证。");

        mxMap.put("缓","一息四至、来去缓怠。湿证，脾虚。");
        mxMap.put("涩","脉细而缓，往来艰涩不畅，如轻刀刮竹。气滞血瘀，精伤血少。");
        mxMap.put("结","脉来缓慢，时见一止，止无定数。阴盛气结，寒痰血瘀。");

        mxMap.put("数","脉率增快，一息脉来五至以上。热证，亦主虚证。");
        mxMap.put("促","脉来数而时一止，止无定数。阳盛实热，气滞血瘀。");
        mxMap.put("疾","一息七至以上，脉来急疾。阳极阴竭，元气将脱。");
        mxMap.put("动","脉形如豆，厥厥动摇，滑数有力。痛，惊。");
        mxMap.put("虚","三部脉举之无力，按之空虚。虚证，多为气血两虚。");
        mxMap.put("微","极细极软，似有似无，至数不明。阳气血诸虚，阳虚危候。");
        mxMap.put("细","脉细如线，但应指明显。气血两虚，诸虚劳损，主湿。");
        mxMap.put("代","脉来一止，止有定数，良久方来。脏气衰微，跌扑损伤，主湿。");
        mxMap.put("弱","柔细而沉。气血不足。");
        mxMap.put("短","首尾俱短，不能满部。有力为气郁，无力为气损。");
        mxMap.put("实","三部脉举按均有力。实证。");
        mxMap.put("滑","往来流利，如珠走盘，应指圆滑。痰饮，食滞，实热。");
        mxMap.put("紧","脉来紧张，状如牵绳转索。寒，痛，宿食。");
        mxMap.put("长","首尾端直，超过本位。阳气有余。");
        mxMap.put("弦","端直而长，如按琴弦。肝胆病，痛证，痰饮，疟疾。");

        int i=0;
        for(String key:mxMap.keySet()){
            mxArr[i] = (i+1)+"."+key+"脉："+mxMap.get(key);
            i++;
        }
    }
}
