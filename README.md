# 医生助手安卓版

#### 介绍
《智慧中医药大脑》是基于知识工程技术，应用人工智能算法开发的；面向医生、医学生和中医药爱好者，用于学习科研目的的中医药“辨证论治”辅助决策系统。
 本项目为调用《智慧中医药大脑》OpenAPI的安卓端。

#### 调用接口
1、	根据性别+年龄+一组症状获取可能关联的其它症状和AI辨证的证型。

2、	根据性别+年龄+一组症状+证型获取AI判断的治法治则。

3、	根据性别+年龄+一组症状+证型获取AI开具的最优化处方。

4、	根据性别+年龄+一组症状+证型+治法获取AI推荐的方剂。

5、	根据性别+年龄+一组症状+证型+治法获取AI推荐的中成药。

6、	根据性别+年龄+一组症状+证型+治法获取AI推荐的食疗方案。

7、	根据性别+年龄+一组症状+证型+治法获取AI推荐的理疗方案。

8、	根据方药ID号+方药类别获取AI推荐的方药详情。

9、	根据性别+年龄+一组症状+证型获取AI推理过程的知识图谱数据。


#### 软件架构
     详情请参考：https://my.oschina.net/mye/blog/5195410
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
